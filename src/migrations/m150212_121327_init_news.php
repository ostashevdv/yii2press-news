<?php

use yii\db\Schema;
use yii\db\Migration;

class m150212_121327_init_news extends Migration
{

    protected $tableOptions;
    protected $colTimestampAttr;

    public function init()
    {
        parent::init();

        switch (\Yii::$app->db->driverName) {
            case 'mysql':
                $this->tableOptions = ' CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB ';
                $this->colTimestampAttr = ' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ';
                break;
            case 'pgsql':
                $this->tableOptions = null;
                $this->colTimestampAttr = ' DEFAULT CURRENT_TIMESTAMP ';
                break;
            default:
                throw new \RuntimeException('Your database is not supported!');
        }
    }

    public function up()
    {
        $this->createTable('{{%news_category}}', [
            'id'                => Schema::TYPE_PK,
            'status'            => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '".\yii2press\news\models\News::STATUS_PUBLISHED_INDEX."' ",
            'name'              => Schema::TYPE_STRING  . " NOT NULL ",
            'slug'              => Schema::TYPE_STRING  . " NOT NULL ",
            'description'       => Schema::TYPE_TEXT    . " NOT NULL DEFAULT '' ",
            'sort'              => Schema::TYPE_INTEGER . " NOT NULL DEFAULT 500",
            'meta_title'        => Schema::TYPE_STRING  . " NOT NULL DEFAULT '' ",
            'meta_description'  => Schema::TYPE_TEXT    . " NOT NULL DEFAULT '' ",
            'meta_keywords'     => Schema::TYPE_STRING  . " NOT NULL DEFAULT '' ",
        ], $this->tableOptions);

        $this->createIndex('uq_news_category_slug', '{{%news_category}}', ['slug'], true);
        $this->createIndex('uq_news_category_name', '{{%news_category}}', ['name'], true);

        $this->createTable('{{%news_item}}', [
            'id'            => Schema::TYPE_PK,
            'slug'          => Schema::TYPE_STRING      .   " NOT NULL",
            'title'          => Schema::TYPE_STRING      .  " NOT NULL",
            'status'        => Schema::TYPE_INTEGER     .   " NOT NULL DEFAULT 1",
            'category_id'   => Schema::TYPE_INTEGER     .   " NOT NULL DEFAULT 1",
            'description'   => Schema::TYPE_TEXT        .   " NOT NULL DEFAULT ''",
            'content'       => Schema::TYPE_TEXT        .   " NOT NULL DEFAULT ''",
            'purify'        => Schema::TYPE_TEXT        .   " NOT NULL DEFAULT ''",
            'image'         => Schema::TYPE_TEXT        .   " NOT NULL DEFAULT ''",
            'use_image'     => Schema::TYPE_INTEGER     .   " NOT NULL DEFAULT 0",
            'image_text'    => Schema::TYPE_STRING      .   " NOT NULL DEFAULT ''",
            'video'         => Schema::TYPE_TEXT        .   " NOT NULL DEFAULT ''",
            'use_video'     => Schema::TYPE_INTEGER     .   " NOT NULL DEFAULT 0",
            'source_url'    => Schema::TYPE_TEXT        .   " NOT NULL DEFAULT ''",
            'rss'           => Schema::TYPE_INTEGER     .   " NOT NULL DEFAULT 1",
            'main'          => Schema::TYPE_INTEGER     .   " NOT NULL DEFAULT 0",
            'important'     => Schema::TYPE_INTEGER     .   " NOT NULL DEFAULT 0",
            'advert'        => Schema::TYPE_INTEGER     .   " NOT NULL DEFAULT 0",
            'created'       => Schema::TYPE_TIMESTAMP   .   $this->colTimestampAttr,
            'published'     => Schema::TYPE_TIMESTAMP   .   " DEFAULT '1981-01-01 00:00:01' ",
            'modify'        => Schema::TYPE_TIMESTAMP   .   " DEFAULT '1981-01-01 00:00:01' ",
            'unpublished'   => Schema::TYPE_TIMESTAMP   .   " DEFAULT '2036-12-31 23:59:59' ",
            'author_id'     => Schema::TYPE_INTEGER     .   "  NOT NULL DEFAULT 1",
            'redactor_id'   => Schema::TYPE_INTEGER     .   "  NOT NULL DEFAULT 1",
            'views'         => Schema::TYPE_INTEGER     .   "  NOT NULL DEFAULT 0 " ,
            'comments'      => Schema::TYPE_INTEGER     .   "  NOT NULL DEFAULT 0 ",
            'gallery_json'  => Schema::TYPE_TEXT        .   " NOT NULL DEFAULT ''",
            'meta_title'    => Schema::TYPE_STRING      .   " NOT NULL DEFAULT ''",
            'meta_keywords' => Schema::TYPE_STRING      .   " NOT NULL DEFAULT ''",
            'meta_description'=> Schema::TYPE_STRING    .   " NOT NULL DEFAULT ''",
        ], $this->tableOptions);

        $this->createIndex('uq_news_item_title', '{{%news_item}}', ['title'], true);
        $this->createIndex('uq_news_item_slug', '{{%news_item}}', ['slug'], true);
        $this->createIndex('ix_news_item_publish', '{{%news_item}}', ['status', 'published']);
        //$this->addForeignKey('fk_news_item_to_category', '{{%news_item}}', 'category_id', '{{%news_category}}', 'id');

        $this->createTable('{{%news_tag}}', [
            'id'            => Schema::TYPE_PK,
            'name'          => Schema::TYPE_STRING  . ' NOT NULL',
            'frequency'     => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
        ], $this->tableOptions);

        $this->createTable('{{%news_tag_assn}}', [
            'news_id'   => Schema::TYPE_INTEGER . ' NOT NULL',
            'tag_id'    => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $this->tableOptions);
        $this->addPrimaryKey('pk_news_tag_assn', '{{%news_tag_assn}}', ['news_id', 'tag_id']);
    }

    public function down()
    {
        //$this->dropForeignKey('fk_news_item_to_category', '{{%news_item}}');
        $this->dropTable('{{%news_item}}');
        $this->dropTable('{{%news_category}}');
        $this->dropTable('{{%news_tag_assn}');
        $this->dropTable('{{%news_tag}}');

        return true;
    }
}
