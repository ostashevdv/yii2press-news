<?php

use yii\db\Schema;
use yii\db\Migration;

class m150601_065201_add_col_publisher_gallery extends Migration
{
    public function up()
    {
        $this->addColumn('{{%news_item}}', 'publisher_id', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1');
        $this->addColumn('{{%news_item}}', 'picture_story', Schema::TYPE_TEXT . " NOT NULL DEFAULT '' ");

        $rows = \yii2press\news\models\News::find()->where("gallery_json != ''")->asArray()->all();



        foreach ($rows as $row) {
            $gallery = explode(',', $row['gallery_json']);

            $new = [];
            foreach($gallery as $sort=>$src) {
                $new[$sort] = [
                    'label' => '',
                    'sort' => $sort,
                    'src' => $src,
                ];
            }

            $gallery = \yii\helpers\Json::encode($new);

            try {
                \Yii::$app->db->createCommand()->update('{{%news_item}}', ['picture_story' => $gallery], ['id' => $row['id']])->execute();
            } catch (\Exception $e) {
                echo $row['id'];
            }
        }

        \Yii::$app->db->createCommand("UPDATE news_item SET publisher_id = author_id;")->execute();

    }

    public function down()
    {
        $this->dropColumn('{{%news_item}}', 'publisher_id');
        $this->dropColumn('{{%news_item}}', 'picture_story');
        return true;
    }
    

}
