<?php
namespace yii2press\news\controllers;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii2press\news\models\Category;

/**
 * Class AdminController
 * @package yii2press\news\controllers
 */
class AdminController extends \yii\web\Controller
{

    /**
     * Список и поиск категорий новостей
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCategoryIndex()
    {
        $searchModel = Yii::$app->getModule('news')->modelMap['CategorySearch'];
        $searchModel = Yii::$container->get($searchModel);
        /** @var \yii2press\news\models\NewsSearch $searchModel */
        $searchModel = new $searchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('category/index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider,]);
    }

    /**
     * Создание категории новостей
     * @return string|Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCategoryCreate()
    {
        $model = Yii::$app->getModule('news')->modelMap['Category'];
        /** @var \yii2press\news\models\Category $model */
        $model = Yii::$container->get($model);

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['category-index']);
        } else {
            return $this->render('category/create', ['model' => $model]);
        }
    }

    /**
     * Редактирование категории новостей
     * @return string|Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCategoryUpdate($id)
    {
        $model = Yii::$app->getModule('news')->modelMap['Category'];
        /** @var \yii2press\news\models\Category $model */
        $model = Yii::$container->get($model);
        $model = $model::findOne(['id'=>$id]);
        if ($model===null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['category-index']);
        } else {
            return $this->render('category/update', ['model' => $model]);
        }
    }














    public function actionNewsIndex()
    {
        $searchModel = Yii::$app->getModule('news')->modelMap['NewsSearch'];
        $searchModel = Yii::$container->get($searchModel);
        /** @var \yii2press\news\models\NewsSearch $searchModel */
        $searchModel = new $searchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('news/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionNewsCreate()
    {
        $model = Yii::$app->getModule('news')->modelMap['News'];
        /** @var \yii2press\news\models\News $model */
        $model = Yii::$container->get($model, [], [
            'scenario' => 'publication',
            'attributes' => [
                'status' => 1,
                'rss' => 1,
                'published' => date("Y-m-d H:i:s")
            ]
        ]);

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['news-index']);
        } else {
            return $this->render('news/create', ['model' => $model]);
        }
    }

    public function actionNewsUpdate($id)
    {
        $model = Yii::$app->getModule('news')->modelMap['News'];
        /** @var \yii2press\news\models\News $model */
        $model = Yii::$container->get($model);
        $model = $model::findOne(['id'=>$id]);
        if ($model===null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['news-index']);
        } else {
            return $this->render('news/update', ['model' => $model]);
        }
    }

    public function actionTagList($query)
    {
        /** @var \yii2press\news\models\Tag $model */
        $model = Yii::$app->getModule('news')->getModel('Tag');
        $tags = $model::find()->select('name')->asArray()->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $tags;
    }


    /**
     * Performs AJAX validation.
     * @param array|Model $models
     * @throws \yii\base\ExitException
     */
    protected function performAjaxValidation($models)
    {
        if (\Yii::$app->request->isAjax) {
            if (is_array($models)) {
                $result = [];
                foreach ($models as $model) {
                    if ($model->load(\Yii::$app->request->post())) {
                        \Yii::$app->response->format = Response::FORMAT_JSON;
                        $result = array_merge($result, ActiveForm::validate($model));
                    }
                }
                echo json_encode($result);
                \Yii::$app->end();
            } else {
                if ($models->load(\Yii::$app->request->post())) {
                    \Yii::$app->response->format = Response::FORMAT_JSON;
                    echo json_encode(ActiveForm::validate($models));
                    \Yii::$app->end();
                }
            }
        }
    }

} 