<div class="panel with-border ">
    <div class="panel-body no-padding">
        <?= \yii\bootstrap\Nav::widget([
            'options' => ['class' => 'nav nav-pills'],
            'encodeLabels' => false,
            'items' => [
                ['label' => 'Добавить', 'url' => ['/news/admin/news-create']],
                ['label' => 'Список', 'url' => ['/news/admin/news-index']],
            ]
        ]);
        ?>
    </div>
</div>
