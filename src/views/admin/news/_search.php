<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii2press\news\models\Category;
?>
<div class="box box-info collapsed-box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">Расширенный поиск</h3>
        <div class="box-tools pull-right">
            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
        <?php $form = ActiveForm::begin([
        'action' => ['news-index'],
        'method' => 'get',
        //'enableAjaxValidation'   => true,
        //'enableClientValidation' => true
        ]);
        ?>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <?= $form->field($model, 'category_id')->dropDownList(Category::getList()) ?>
                </div>
                <div class="form-group">
                    <label class="control-label"> Период создания</label>
                    <?= \kartik\date\DatePicker::widget([
                        'model' => $model,
                        'attribute' => 'published_from',
                        'attribute2' => 'published_to',
                        'type' => \kartik\date\DatePicker::TYPE_RANGE,
                        'separator' => 'по',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]) ?>
                </div>
                <?= $form->field($model, 'title')->textInput()?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'important')->checkbox() ?>
            </div>
        </div>


        <div class="form-group">
            <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Сбросить', ['class' => 'btn btn-danger', 'onclick'=>"document.getElementById('news-search').reset()"]) ?>

        </div>
        <?php ActiveForm::end(); ?>
        <?php ?>
    </div><!-- /.box-body -->
</div>