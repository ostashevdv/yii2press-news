<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="news-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation'   => true,
        'enableClientValidation' => true
    ]);
    echo $form->errorSummary($model, ['class' => 'bg-danger', 'style' => 'padding: 10px; margin-bottom: 10px;' ]);

    echo \yii\bootstrap\Tabs::widget([
        'encodeLabels' => false,
        'items' => [
            [
                'label' => '<i class="fa fa-newspaper-o"></i> <br> Основное  ',
                'linkOptions' => ['class' => 'btn btn-md btn-flat', 'title' => 'Основное'],
                'options' => ['style' => 'padding-top: 15px'],
                'content' => $this->render('_form_prop_main', ['model' => $model, 'form' => $form]),
            ],
            [
                'label' => '<i class="fa fa-wrench"></i> <br> Дополнительно',
                'linkOptions' => ['class' => 'btn btn-md btn-flat', 'title' => 'Дополнительно'],
                'options' => ['style' => 'padding-top: 15px'],
                'content' => $this->render('_form_prop_extra', ['model' => $model, 'form' => $form])
            ],
            [
                'label' => '<i class="fa fa-globe"></i> <br> SEO',
                'linkOptions' => ['class' => 'btn btn-md btn-flat', 'title' => 'SEO оптимизация'],
                'options' => ['style' => 'padding-top: 15px'],
                'content' => $this->render('_form_prop_seo', ['model' => $model, 'form' => $form])
            ],
        ]
    ]);
    ?>
    <div class="form-group" style="margin-top: 15px;">
        <?= Html::submitButton('<i class="fa fa-check"></i>&nbsp;Сохранить', ['class' =>'btn btn-success']) ?>
        <?= Html::a('<i class="fa fa-ban"></i>&nbsp;Отмена', ['news-index'], ['class' =>'btn btn-link']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>