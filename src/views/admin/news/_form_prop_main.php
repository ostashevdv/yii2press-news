<?php
/**
 * @var \yii\web\View $this
 * @var \yii2press\news\models\News $model
 */
?>

<div class="row">

    <div class="col-xs-12 col-md-8">
        <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'description')->textarea(['rows'=>6]) ?>
        <?php
        $bBundle = Yii::$app->assetManager->getBundle('app\assets\backend\BackendBundle');
        echo \himiklab\ckeditor\CKEditor::widget([
            'model' => $model,
            'attribute' => 'content',
            'useBrowserSpellChecker' => true,
            'editorOptions' => [
                'height' => '400px',
                'customConfig' => $bBundle->baseUrl . '/ckeditor/config.ckeditor.js',
                'filebrowserBrowseUrl' => '/elfinder/manager',
            ]
        ]);
        ?>

        <br/>
        <?= $form->field($model, 'tagValues')->widget('\\dosamigos\\selectize\\SelectizeTextInput', [
            'loadUrl' => ['tag-list'],
            'clientOptions' => [
                'plugins' => ['remove_button'],
                'valueField' => 'name',
                'labelField' => 'name',
                'searchField' => ['name'],
                'create' => true,
                'maxItems' => 6
            ],]); ?>

    </div>

    <div class="col-xs-12 col-md-4">

        <div class="panel panel-default">
            <div class="panel-heading">Настройки видимости</div>
            <div class="panel-body">
                <?= $form->field($model, 'category_id')->dropDownList(\yii2press\news\models\Category::getList()) ?>
                <?= $form->field($model, 'status')->dropDownList($model::getStatusList())?>

                <div class="form-group field-news-published">
                    <label for="news-published">Дата</label>
                    <?= \kartik\datetime\DateTimePicker::widget([
                        'model' => $model,
                        'attribute' => 'published',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd hh:ii'
                        ]
                    ]) ?>
                </div>

                <?php if(!$model->isNewRecord) : ?>
                <?= $form->field($model, 'publisher_id')->dropDownList($model->allowedPublishers) ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Изображение</div>
            <div class="panel-body">
                <?= \mihaildev\elfinder\InputFile::widget([
                    'model' => $model,
                    'attribute' => 'image',
                    'buttonName'    => 'Загрузить',
                    'language'      => 'ru',
                    'controller'    => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                    'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                    'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span> </div>',
                    'options'       => [
                        'class' => 'form-control',
                        'onchange' => "$('#news-img-preview').attr('src', this.value);"
                    ],
                    'buttonOptions' => ['class' => 'btn btn-default'],
                    'multiple'      => false      // возможность выбора нескольких файлов
                ]); ?>
                <?= $form->field($model, 'use_image', ['options' => ['style'=>'margin-top:15px;']])->checkbox(['label' => 'Выводить в ленте']) ?>

                <img id='news-img-preview' src="<?=$model->image?>" alt="" style="display: block; width: 100%;"/>

                <?= $form->field($model, 'image_text')->textarea(['rows'=>2]); ?>

            </div>
        </div>


        <div class="panel panel-default">
            <div class="panel-heading">Дополнительно</div>
            <div class="panel-body">
                <?= $form->field($model, 'main')->checkbox(['labelOptions' => ['class'=>'checkbox-inline']]) ?>
                <?= $form->field($model, 'important')->checkbox(['labelOptions' => ['class'=>'checkbox-inline']]) ?>
                <?= $form->field($model, 'rss')->checkbox(['labelOptions' => ['class'=>'checkbox-inline']]) ?>
                <?= $form->field($model, 'advert')->checkbox(['labelOptions' => ['class'=>'checkbox-inline']]) ?>
                <?=  $form->field($model, 'source_url')->textInput() ?>
            </div>
        </div>

    </div>
</div>