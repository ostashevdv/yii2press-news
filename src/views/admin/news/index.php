<?php
/**
 * Список, фильтрация, поиск по новостям
 * -------------------------------------------------------------
 * @var \yii\web\View $this
 * @var \yii2press\news\models\Category $model
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii2press\news\models\Category;

$this->title = 'Модуль новостей';
$this->params['breadcrumbs'] = ['Новости'];
?>

<div class="row">
    <div class="col-xs-12">
        <?= $this->render('/admin/_menu') ?>
    </div>
    <div class="col-xs-12">
        <?= $this->render('_search', ['model' => $searchModel]) ?>
        <div class="box box-primary">
            <div class="box-header"> <h4 class=""> Список новостей </h4> </div>
            <div class="box-body category-index">
                <?php Pjax::begin() ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'layout' => '{summary}<div class="table-responsive">{items}</div>{pager}',
                    'rowOptions' => function ($model, $key, $index, $grid){
                        /** @var \yii2press\news\models\News $model */
                        $class = [];
                        if ($model->status==$model::STATUS_UNPUBLISHED || $model->status==$model::STATUS_PUBLISHED_PRIVATE) {
                            Html::addCssClass($class, 'text-muted');
                        } elseif ($model->main==1 || $model->important==1) {
                            Html::addCssClass($class, 'text-danger');
                        }
                        return $class;
                    },
                    'columns' => [
                        ['class' => 'yii\grid\DataColumn', 'attribute' => 'id', 'options' => ['style' => 'width: 80px']],
                        [
                            'class' => 'yii\grid\DataColumn',
                            'attribute' => 'published',
                            'options' => ['style' => 'width: 120px'],
                            'value' => function($data) {
                                return (new DateTime($data->published))->format("d.m H:i");
                            },
                            'filter' => \kartik\date\DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'published',
                                'value' => date("Y-m-d"),
                                'type' => 1,

                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]),


                        ],
                        [
                            'class' => 'yii\grid\DataColumn',
                            'attribute' => 'category_id',
                            'value'=>'category.name',
                            'filter' => Html::activeDropDownList($searchModel, 'category_id', Category::getList(), ['class' => 'form-control'])
                        ],
                        ['class' => 'yii\grid\DataColumn', 'attribute' => 'title', 'format' => 'raw', 'value' => function($data){
                            $type = '';
                            $type .= $data->video ? "<i class='fa fa-video-camera'></i> &nbsp;" : '';
                            $type .= $data->gallery_json ? "<i class='fa fa-camera'></i> &nbsp;" : '';
                            return $data->title.Html::tag('div',$type);
                        }],
                        [
                            'class' => 'yii\grid\DataColumn',
                            'header' => 'Редактор / Автор',
                            'format' => 'raw',
                            'value' => function($data) {
                                return $data->redactor->username .' / '. $data->author->username;
                            }
                        ],

                        [
                            'class' => 'yii\grid\DataColumn',
                            'format' => 'raw',
                            'options' => ['style' => 'width: 12px'],
                            'value'=>function ($data) {
                                $out = $data->main ? Html::tag('div', 'главная',['class'=>'label bg-red pull-right']) : '';
                                $out .= $data->important ? Html::tag('div', 'важная',['class'=>'label bg-yellow pull-right']) : '';
                                $out .= $data->advert ? Html::tag('div', 'реклама',['class'=>'label bg-green pull-right']) : '';
                                return $out;
                            },

                        ],
                        [
                            'class' => 'yii\grid\DataColumn',
                            'label' => 'Пр-ы',
                            'options' => ['style' => 'width: 20px'],
                            'attribute' => 'views',
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}{public-view}',
                            'options' => [
                                'style' => 'width: 100px',
                            ],
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="fa fa-pencil"></span>',
                                        Url::to(['/news/admin/news-update', 'id' => $model->id]),
                                        ['class' => 'btn btn-sm btn-primary btn-flat']);
                                },
                                'public-view' => function($url, $model) {
                                    return Html::a(
                                        '<span class="fa fa-play"></span>',
                                        Url::to(['/news/default/view', 'id' => $model->id]),
                                        ['class' => 'btn btn-sm btn-default btn-flat', 'target' => '_blank']
                                    );
                                }
                            ]
                        ],
                    ],
                ]); ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>