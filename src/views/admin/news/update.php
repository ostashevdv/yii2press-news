<?php
/**
 * View редактирования новости
 * -------------------------------------------------------------
 * @var \yii\web\View $this
 * @var \yii2press\news\models\News
 */
$this->title = 'Модуль новостей';
$this->params['breadcrumbs'][] = ['label'=> 'Новости', 'url' => ['/news/admin/news-index']];
$this->params['breadcrumbs'][] = ['label'=> 'Редактировать'];
?>

<div class="row">
    <div class="col-xs-12">
        <?= $this->render('/admin/_menu') ?>
    </div>
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header"> <h5 class="box-title"> Редактировать новость </h5> </div>
            <div class="box-body">
                <?= $this->render('_form', ['model' => $model]); ?>
            </div>
        </div>
    </div>
</div>