
<?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'meta_title')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'meta_description')->textarea() ?>