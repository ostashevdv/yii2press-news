<?php
/**
 * @var \yii\web\View $this
 * @var $form \yii\widgets\ActiveForm
 */
?>
<div class="row">
    <div class="col-xs-10"><?= $form->field($model, 'video')->textarea(['rows' => 3]) ?></div>
    <div class="col-xs-2"><?=\yii\helpers\Html::activeCheckbox($model, 'use_video')?> </div>
</div>

<?= \yii2press\jgmanager\widgets\Input::widget(['model'=>$model, 'attribute'=>'gallery']) ?>