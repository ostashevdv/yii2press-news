<?php
/**
 * @var \yii\web\View $this
 * @var \yii2press\news\models\Category $model
 */
?>

<?= $form->field($model, 'status')->checkbox(['class' => 'minimal', 'value' => 1]) ?>
    <div class="row">
        <div class="col-md-10"><?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?></div>
        <div class="col-md-2"><?= $form->field($model, 'sort')->input('number', ['maxlength' => 4, 'size' => '4', 'value' => 500]) ?></div>
    </div>
<?= $form->field($model, 'description')->textarea() ?>