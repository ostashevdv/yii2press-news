<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model yii2press\news\models\Category */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="category-form">
    <?php
    $form = ActiveForm::begin([
        'enableAjaxValidation'   => true,
        'enableClientValidation' => true
    ]);

    echo $form->errorSummary($model, ['class' => 'bg-danger', 'style' => 'padding: 10px; margin-bottom: 10px;' ]);

    echo \yii\bootstrap\Tabs::widget([
        'encodeLabels' => false,
        'items' => [
            [
                'label' => '<i class="fa fa-newspaper-o"></i> <br> Основное  ',
                'linkOptions' => ['class' => 'btn btn-md btn-flat', 'title' => 'Основное'],
                'options' => ['style' => 'padding-top: 15px'],
                'content' => $this->render('_form_prop_main', ['model' => $model, 'form' => $form]),
            ],
            [
                'label' => '<i class="fa fa-wrench"></i> <br> SEO',
                'linkOptions' => ['class' => 'btn btn-md btn-flat', 'title' => 'SEO оптимизация'],
                'options' => ['style' => 'padding-top: 15px'],
                'content' => $this->render('_form_prop_seo', ['model' => $model, 'form' => $form])
            ],
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>