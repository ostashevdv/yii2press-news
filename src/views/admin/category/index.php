<?php
/**
 * Created by Ostashev Dmitriy <ostashevdv@gmail.com>
 * Date: 17.02.2015 Time: 15:37
 * -------------------------------------------------------------
 * @var \yii\web\View $this
 * @var \yii2press\news\models\Category $model
 */
use yii\helpers\Html;
$this->title = 'Модуль новостей';
$this->params['breadcrumbs'][] = ['label'=> 'Категории новостей'];
?>

<div class="row">
    <div class="col-md-2">
        <?= $this->render('/admin/_menu') ?>
    </div>
    <div class="col-md-10">
        <div class="box box-primary">
            <div class="box-header"> <h4 class=""> Список категорий </h4> </div>
            <div class="box-body category-index">
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'rowOptions' => function ($model, $key, $index, $grid){
                        return ['class'=> $model->status ? : 'danger'];
                    },
                    'columns' => [
                        'id',
                        'name',
                        'slug',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '<div class="btn-group">{update}</div>',
                            'options' => [
                                'style' => 'width: 60px',
                            ],
                            'buttons' => [
                                'update' => function($url, $model){
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['/news/admin/category-update', 'id' => $model->id], [
                                        'class' => 'btn btn-flat btn-warning'
                                    ]);
                                },

                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>