<?php
/**
 * Created by Ostashev Dmitriy <ostashevdv@gmail.com>
 * Date: 17.02.2015 Time: 15:37
 * -------------------------------------------------------------
 * @var \yii\web\View $this
 * @var \yii2press\news\models\News
 */
$this->title = 'Модуль новостей';
$this->params['breadcrumbs'][] = ['label'=> 'Категории новостей', 'url' => ['/news/admin/category-index']];
$this->params['breadcrumbs'][] = ['label'=> 'Редактировать'];
?>

<div class="row">
    <div class="col-md-2">
        <?= $this->render('/admin/_menu') ?>
    </div>
    <div class="col-md-10">
        <div class="box box-primary">
            <div class="box-header"> <h5 class="box-title"> Редактировать категорию новостей </h5> </div>
            <div class="box-body">
                <?= $this->render('_form', ['model' => $model]); ?>
            </div>
        </div>
    </div>
</div>