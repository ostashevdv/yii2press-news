<?php
/**
 * основной модуль новостей проекта yii2press
 */

namespace yii2press\news;


class Module extends \yii\base\Module
{

    /** @var array список переопределяемых моделей */
    public $modelMap = [];

    public $urlRules = [];

    public function getModel($modelName, $params=[], $config=[])
    {
        return \Yii::$container->get($this->modelMap[$modelName], $params, $config);
    }

} 