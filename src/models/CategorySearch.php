<?php
/**
 * Created by Ostashev Dmitriy <ostashevdv@gmail.com>
 * Date: 17.02.2015 Time: 15:31
 * -------------------------------------------------------------
 */

namespace yii2press\news\models;


use yii\data\ActiveDataProvider;

class CategorySearch extends \yii\base\Model
{
    public $name;
    public $slug;
    public $status;

    public function rules()
    {
        return [
            [['name', 'slug'], 'string'],
            ['status', 'integer']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $model = \Yii::$app->getModule('news')->modelMap['Category'];
        /** @var $model \yii2press\news\models\Category */
        $model = \Yii::$container->get($model);
        /** @var  $query \yii\db\ActiveQuery */
        $query = $model::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere([
            'status' => $this->status,
        ]);
        return $dataProvider;
    }
} 