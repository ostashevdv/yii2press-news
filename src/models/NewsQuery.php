<?php
namespace yii2press\news\models;


class NewsQuery extends \yii\db\ActiveQuery
{
    public function behaviors()
    {
        return [
            '\\creocoder\\taggable\\TaggableQueryBehavior',
        ];
    }

    /**
     * Только опубликованные новости
     * @return $this
     */
    public function available()
    {
        //$this->andWhere(['<', 'published',' NOW()']);
        $this->andWhere(['>=', 'status', News::STATUS_PUBLISHED_PRIVATE]);
        //$this->andWhere('status > 0 and published < NOW()');
        return $this;
    }

    /**
     * ограничивает количество публикаций за последние N дней
     * @param int $days
     * @return $this
     */
    public function daysAgo($n = 3)
    {
        $this->andWhere(['published > :time'], [':time' => strtotime("-{$n} day")]);
        return $this;
    }
} 