<?php

namespace yii2press\news\models;

use Yii;


/**
 * This is the model class for table "tag".
 *
 * @property integer $id
 * @property string $name
 * @property integer $frequency
 */
class Tag extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'news_tag';
    }

    public function rules()
    {
        return [
            [['frequency'], 'integer'],
            [['name'], 'string', 'max' => 128]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Тег',
            'frequency' => 'Частота',
        ];
    }

}
