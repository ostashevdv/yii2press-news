<?php
namespace yii2press\news\models;


use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class News
 * @package yii2press\models
 *
 * @property integer $id
 * @property string  $slug
 * @property string  $title
 * @property integer $status
 * @property integer $category_id
 * @property string  $description
 * @property string  $content
 * @property string  $purify
 * @property string  $image
 * @property integer $use_image
 * @property string  $image_text
 * @property string  $video
 * @property integer $use_video
 * @property string  $source_url
 * @property integer $rss
 * @property integer $main
 * @property integer $important
 * @property integer $advert
 * @property string  $created
 * @property string  $published
 * @property string  $modify
 * @property string  $unpublished
 * @property integer $author_id
 * @property integer $redactor_id
 * @property integer $publisher_id
 * @property integer $views
 * @property integer $comments
 * @property string  $gallery_json
 * @property string  $picture_story
 * @property string  $meta_title
 * @property string  $meta_keywords
 * @property string  $meta_description
 *
 * @property Category $category
 */
class News extends \yii\db\ActiveRecord
{
    /** Скрыто */
    const STATUS_UNPUBLISHED = 0;

    /** Доступно только по прямой ссылке, не выводится в лентах */
    const STATUS_PUBLISHED_PRIVATE = 10;

    /** Выводится в ленте категории */
    const STATUS_PUBLISHED_PUBLIC = 20;

    /** Выводится в ленте на главной */
    const STATUS_PUBLISHED_INDEX = 30;

    public static function getStatusList()
    {
        return [
            self::STATUS_PUBLISHED_INDEX => 'На главной',
            self::STATUS_PUBLISHED_PUBLIC => 'В категории',
            self::STATUS_PUBLISHED_PRIVATE => 'Приватно',
            self::STATUS_UNPUBLISHED => 'Скрыто',
        ];
    }

    public $modelMap;

    public $deniedCategories = [9, 10, 11];

    public function rules()
    {
        return [
            [['title', 'slug', 'description', 'content', 'purify', 'image', 'image_text', 'video', 'source_url', 'gallery_json', 'picture_story', 'meta_title', 'meta_keywords', 'meta_description'], 'string'],
            [['status', 'category_id', 'use_image', 'use_video', 'rss', 'main', 'important', 'advert', 'author_id', 'redactor_id', 'publisher_id', 'views', 'comments'], 'integer'],
            [['created', 'published', 'modify', 'unpublished', 'tagValues', 'publisher_id' ], 'safe'],

            [['title', 'description', 'content'], 'required'],
            [['title', 'slug'], 'unique'],
            [['slug', 'description', 'image_text', 'meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
            [['use_image', 'use_video', 'main', 'important', 'advert',  'comments', 'views', 'comments'], 'default', 'value' => 0],
            [['status', 'rss', ], 'default', 'value' => 0],

            [['status'], 'default', 'value' => self::STATUS_PUBLISHED_INDEX, 'on' => ['publication']],
            [['rss'], 'default', 'value' => 1, 'on' => ['publication']],
            [['title'], 'string', 'max' => 180, 'on' => ['publication']],

            ['category_id', 'exist', 'targetClass' => 'yii2press\\news\\models\\Category', 'targetAttribute'=>'id' ]
            

        ];
    }

    public function beforeValidate()
    {
        $this->purify = $this->content;
        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        $this->meta_title = $this->title;
        $this->meta_description = $this->description;
        return parent::beforeSave($insert);
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => '\\yii\\behaviors\\SluggableBehavior',
                'attribute' => 'title',
                'immutable' => true,
            ],
            'timestamp' => [
                'class' => '\\yii\\behaviors\\TimestampBehavior',
                'attributes' => [

                    parent::EVENT_BEFORE_INSERT => ['modify', 'created'],
                    parent::EVENT_BEFORE_UPDATE => ['modify'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'purify' => [
                'class' => '\\yii2press\\core\\behaviors\\PurifierBehavior',
                'textAttributes' => [
                    parent::EVENT_BEFORE_INSERT => ['title', 'description', 'meta_title', 'meta_keywords', 'meta_description', 'purify', 'source_url'],
                    parent::EVENT_BEFORE_UPDATE => ['title', 'description', 'meta_title', 'meta_keywords', 'meta_description', 'purify', 'source_url'],
                ],
                'attributes' => [
                    parent::EVENT_BEFORE_INSERT => ['content'],
                    parent::EVENT_BEFORE_UPDATE => ['content'],
                ],
                'purifierOptions' => [
                    'content' => [
                        'HTML.Allowed'         => 'strong, em, ul, ol, li, img[src], a[href], p, span, h2, h3, h4, h5, h6',
                        'HTML.Trusted'         => TRUE,
                        'HTML.TidyAdd'         => [],
                        'HTML.TidyLevel'       => 'heavy',
                        'HTML.SafeIframe'      => true,
                        'URI.SafeIframeRegexp' => '%^http://(www.youtube(?:-nocookie)?.com/embed/|player.vimeo.com/video/)%',
                    ]
                ]
            ],
            'blameable' => [
                'class' => '\\yii\\behaviors\\BlameableBehavior',
                'attributes' => [
                    parent::EVENT_BEFORE_INSERT => ['author_id', 'publisher_id', 'redactor_id'],
                    parent::EVENT_BEFORE_UPDATE => ['redactor_id'],
                ],
            ],
            'taggable' => [
                'class' => '\\creocoder\\taggable\\TaggableBehavior'
            ],
            'gallery' => [
                'class' => 'yii2press\\jgmanager\\behaviors\\GalleryBehavior',
                'attribute' => 'picture_story',
            ]

        ];
    }

    public function init()
    {
        $this->modelMap = Yii::$app->getModule('news')->modelMap;
        return parent::init();
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return \app\modules\news\models\NewsQuery | \yii2press\news\models\NewsQuery | \yii\db\ActiveQuery
     */
    public static function find()
    {
        $model = Yii::$app->getModule('news')->modelMap['NewsQuery'];
        return new $model(get_called_class());
    }

    public static function tableName()
    {
        return '{{%news_item}}';
    }

    public function isDenied()
    {
        $post = (new \DateTime($this->published))->modify('+48 hour');
        $now = (new \DateTime());
        if (in_array($this->category_id, $this->deniedCategories) && $post > $now && !Yii::$app->user->can('subscriber')) {
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery Возвращает категорию публикации
     */
    public function getCategory()
    {
        return $this->hasOne($this->modelMap['Category'], ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery Возвращает создателя публикации
     */
    public function getAuthor()
    {
        return $this->hasOne($this->modelMap['User'], ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery Возвращает последнего редактора публикации
     */
    public function getRedactor()
    {
        return $this->hasOne($this->modelMap['User'], ['id' => 'redactor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery Возвращает автора публикации
     */
    public function getPublisher()
    {
        return $this->hasOne($this->modelMap['User'], ['id' => 'publisher_id']);
    }

    /**
     * @return array Список доступных авторов
     * @throws \yii\base\InvalidConfigException
     */
    public function getAllowedPublishers()
    {
        /** @var \dektrium\user\models\User $model */
        $model = Yii::createObject($this->modelMap['User']);
        $model = $model::find()->where(['role' => ['redactor', 'admin', 'manager', 'superadmin']])->asArray()->all();
        return \yii\helpers\ArrayHelper::map($model, 'id', 'username');
    }

    /**
     * @return \yii\db\ActiveQuery Возвращает теги публикации
     */
    public function getTags()
    {
        return $this->hasMany($this->modelMap['Tag'], ['id' => 'tag_id'])->viaTable('{{%news_tag_assn}}', ['news_id' => 'id']);
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'title' => 'Заголовок',
            'status' => 'Видимость',
            'category_id' => 'Категория',
            'description' => 'Описание',
            'content' => 'Содержание',
            'purify' => 'Purify',
            'image' => 'Изображение',
            'use_image' => 'Исп. изобр.',
            'image_text' => 'Подпись к изобр.',
            'video' => 'Video',
            'use_video' => 'Исп. видео',
            'source_url' => 'URL источника',
            'rss' => 'Rss',
            'main' => 'Главная',
            'important' => 'Важная',
            'advert' => 'Реклама',
            'created' => 'Создано',
            'published' => 'Опубликованно',
            'modify' => 'Отредактировано',
            'unpublished' => 'Скрыто',
            'author_id' => 'Автор',
            'redactor_id' => 'Редактор',
            'publisher_id' => 'Автор публ.',
            'views' => 'Просмотры',
            'comments' => 'Комментарии',
            'gallery_json' => 'Галлерея',
            'picture_story' => 'Галлерея',
            'tagValues' => 'Теги',
            'meta_title' => 'SEO title',
            'meta_keywords' => 'SEO keywords',
            'meta_description' => 'SEO description',
        ];
    }
} 