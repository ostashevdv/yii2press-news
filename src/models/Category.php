<?php
namespace yii2press\news\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property integer $status
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property integer $sort
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 *
 * @property News[] $news
 */
class Category extends ActiveRecord
{

    const STATUS_PUBLISHED = 1;
    const STATUS_UNPUBLISHED = 0;


    /**
     * @return array массив доступных статусов
     */
    public function getStatusList()
    {
        return [
            self::STATUS_PUBLISHED => 'Опубликованно',
            self::STATUS_UNPUBLISHED => 'Скрыто',
        ];
    }

    public $modelMap;

    public function rules()
    {
        return [
            [['name', 'slug', 'description', 'meta_title', 'meta_description', 'meta_keywords'], 'string'],
            [['status', 'sort'], 'integer'],

            [['name', 'slug'], 'unique'],
            [['name'], 'required'],

            [['sort'], 'default', 'value' => 500],
            [['status'], 'default', 'value' => 1],
        ];
    }

    public function beforeSave($insert)
    {
        $this->meta_title = $this->name;
        $this->meta_description = $this->description;
        return parent::beforeSave($insert);
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => '\\yii\\behaviors\\SluggableBehavior',
                'attribute' => 'name',
                'immutable' => true,
            ],
            'purify' => [
                'class' => '\\yii2press\\core\\behaviors\\PurifierBehavior',
                'textAttributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['name', 'description',],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['name', 'description',],
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();
        $this->modelMap = Yii::$app->getModule('news')->modelMap;
    }

    /**
     * @return \app\modules\news\models\CategoryQuery | \yii2press\news\models\CategoryQuery | \yii\db\ActiveQuery
     */
    public static function find()
    {
        $model = Yii::$app->getModule('news')->modelMap['CategoryQuery'];
        return new $model(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery Новости категории
     */
    public function getNews()
    {
        return $this->hasMany($this->modelMap['News'], ['category_id' => 'id']);
    }

    /**
     * @return array Список активных категорий
     */
    public static function getList($append=[0=>'Без категории'])
    {
        $arr = self::find()->select(['id','name'])->where(['status' => self::STATUS_PUBLISHED])->orderBy(['sort' => SORT_ASC])->asArray()->all();
        $arr = ArrayHelper::map($arr, 'id', 'name');
        return ArrayHelper::merge($append, $arr);
    }

    /**
     * @return array Массив именований полей
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Видимость',
            'name' => 'Название',
            'slug' => 'Алиас',
            'description' => 'Описание',
            'sort' => 'Порядок',
            'meta_title' => 'SEO title',
            'meta_description' => 'SEO description',
            'meta_keywords' => 'SEO keywords',
        ];
    }

    public static function tableName()
    {
        return '{{%news_category}}';
    }
}
