<?php

namespace yii2press\news\models;


use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

class NewsSearch extends \yii\base\Model
{
    public $id;
    public $status;
    public $category_id;
    public $title;
    public $content;
    public $source_url;
    public $important;
    public $main;
    public $author_id;
    public $redactor_id;
    public $published;
    public $published_from;
    public $published_to;
    public $created_from;
    public $created_to;

    public function rules()
    {
        return [
            [['id', 'status', 'category_id', 'main', 'important', 'author_id', 'redactor_id'], 'integer'],
            [['title', 'content', 'source_url', 'published', 'created_from', 'created_to', 'published_from', 'published_to', ], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'status' => 'Статус',
            'category_id' => 'Категория',
            'title' => 'Заголовок',
            'main' => 'Главная',
            'important' => 'Важная'
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $model = \Yii::$app->getModule('news')->modelMap['News'];
        /** @var $model \yii2press\news\models\News */
        $model = \Yii::$container->get($model);
        /** @var  $query \yii\db\ActiveQuery */
        $query = $model::find()->with(['category', 'redactor', 'author']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['published'=>SORT_DESC, 'id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->published_from) && !empty($this->published_to)) {
            $query->andWhere('published > :from AND published < :to', [
                ':from' => (new \DateTime($this->published_from))->format("Y-m-d 00:00:00"),
                ':to' => (new \DateTime($this->published_to))->format("Y-m-d 23:59:59"),
            ]);
        } else {
            if($this->published) {
                $query->andWhere('published > :from AND published < :to', [
                    ':from' => date("Y-m-d 00:00:00", strtotime($this->published)),
                    ':to' => date("Y-m-d 23:59:59", strtotime($this->published))
                ]);
            } else {
                $query->andWhere('published < :to', [':to' => date("Y-m-d 23:59:59")]);
            }
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'category_id' => $this->category_id==0 ? null : $this->category_id,
            //'main' => $this->main,
            //'important' => $this->important,
        ])->andFilterWhere(['like', 'title', $this->title]);


        return $dataProvider;
    }

} 