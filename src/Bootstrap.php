<?php

namespace yii2press\news;


use yii\base\BootstrapInterface;
use yii\base\InvalidConfigException;
use yii\i18n\PhpMessageSource;
use yii\web\GroupUrlRule;
use yii\console\Application as ConsoleApplication;


class Bootstrap implements BootstrapInterface
{
    /** @var array Model's map */
    private $_modelMap = [
        'Category' => 'yii2press\\news\\models\\Category',
        'CategorySearch' => 'yii2press\\news\\models\\CategorySearch',
        'CategoryQuery' => 'yii2press\\news\\models\\CategoryQuery',

        'News' => 'yii2press\\news\\models\\News',
        'NewsSearch' => 'yii2press\\news\\models\\NewsSearch',
        'NewsQuery' => 'yii2press\\news\\models\\NewsQuery',

        'Tag' => 'yii2press\\news\\models\\Tag'
    ];


    public function bootstrap($app)
    {
        /** @var $module Module */
        if ($app->hasModule('news') && ($module = $app->getModule('news')) instanceof Module) {
            $this->_modelMap = array_merge($this->_modelMap, $module->modelMap);
            foreach ($this->_modelMap as $name => $definition) {
                $class = "yii2press\\news\\models\\" . $name;
                \Yii::$container->set($class, $definition);
                $modelName = is_array($definition) ? $definition['class'] : $definition;
                $module->modelMap[$name] = $modelName;
            }


            if ($app instanceof ConsoleApplication) {
                $module->controllerNamespace = 'yii2press\news\commands';
            }

        }
        
    }
}