yii2press-news
==============
Основной модуль управления новостями проекта yii2press
Модуль имеет гибкие настройки конфигурации, что позволяет избежать лишнего переопределения классов в приложении.

#Оглавление
1. Установка
2. Настройка
3. Использование


## Расширение моделей
1. Сначала расширьте модель модуля:

```php
namespace app\modules\news\models;

class News extends \yii2press\news\models\News
{
    public $extendProperty = null;
}
```

2. Пропишите в конфигурации модуля:

```php
...
'modules' => [
    'news' => [
        'class' => '\yii2press\news\Module,
        'modelMap' => [
            'News' => 'app\modules\news\models\News'
        ]
    ]
]
```